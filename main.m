%We solve the following system (Compile in TeX):

% -\frac{\eta}{\xi L^2}\partial_{yy}\sigma+\sigma=-Ef_e(\rho/L)+\chi\f_c(\rho/L,c/L)
% v=\frac{1}{\xi L}\partial_y\sigma+D_m*(Brownian sheet)
% \partial_t\rho+\frac{1}{L}\partial_y[\rho v-\frac{D_{\rho}}{L}\partial_y\rho]=LS_{\rho}(\rho/L))
% \partial_tc_a+\frac{1}{L}\partial_y[c_av-\frac{D_{ca}}{L}\partial_yc_a]=k_a(y)c_d-k_d(y)c_a
% \partial_tc_d+\frac{1}{L}\partial_y[c_dv-\frac{D_{cd}}{L}\partial_yc_d]=-k_a(y)c_d+k_d(y)c_a
% with bounday conditions,
% v(0,t)=v(1,t)=0
% \partial_yc_a(0,t)=\partial_yc_a(1,t)=0
% \partial_yc_d(0,t)=\partial_yc_d(1,t)=0

% \rho is the actin density, $c_a$ the concentration of attached motors,
% c_d the concentraction of detached motor, v the velocity of actin, sigma
% the stress in the actin meshwork. Notice D_rho is interpreted as a non
% local souce term regularizing the actin density. One may also put a non
% local stress: -Ef_e(\rho/L)+k/L^2\partial_{yy}\rho to get the same type of
% behavior. Maybe this is actually better ?


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PHYSICAL PARAMETERS %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

T_final       =0.5;            % final integration time   (Stop the code with CTRL C if found too long, or increase if too short. Than plot the results with the command visu;)       
T_cut = 0.1; % time when the cut is made;     




% inital conditions
L             =1.0; % curvilinear length between the two poles  
ca_handle     = @(x) unifpdf(x,0,L)*20+ sin(20*L*x+ cos(20*L*x))/(0.5*L) ;   %initial distribution of attached motors
cd_handle     = @(x) unifpdf(x,0,L)*20;   %initial distribution of dettached motors
rho_handle    = @(x) unifpdf(x,0,L)*20;   %initial distribution of actin


% mechanics
eta           =1;            % viscosity of the cortex 
xi            =0.5;         % friction with the (sort of static ?) membrane  
E             =1;            % elastic modulus of the actine meshwork (or compressibility)         
chi           =0.1;          % contractility modulus
Diffuca        =0.0001;            % diffusion coefficient of attached mysoin
Diffucd        =1;            % diffusion coefficient of the detached myosin
Diffurho      =0.0001;            % diffusion coefficient of actin (interpretation as a non local source term)
rho_e         =1;            % 'reference' actin density of elasticity
c_c           =1;            % 'reference' myosin concentration of contraction

% chemistry
rho_0         =1.0;            % target density of actin
tau           =1.0;            % tunover time of actin
k_on          =@(y) 1.000 ; %+1.0*exp(-(y-0.5).^2/0.05);                    % rate of myosin attachement (here centered at the equator)
k_off         =@(y) 10.0*(exp(-y.^2/0.05)+exp(-(y-1.0).^2/0.05));  % rate of myosin detachement (here high at the poles)

%mechanical noise (small biological gaussian noise in the force equation).
D_m         =1e-5;          % Small Gaussian noise on c

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% RHEOLOGY FUNCTIONS  %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

f_e=@(rho)     0;%rho/rho_e; % elastic  compressibility, here infinite compressibility (f_e=0)
f_c= @(rho,c)  c/c_c;     % contraction (here only depending on attached motors)
S_rho= @(rho) (rho_0-rho)/tau;        % source term of actin

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% NUMERICAL PARAMETERS %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dt_jump=100;                    % number of jumped snapshots of the solution before one is save
N=101;                          % number of space points 
CFL=0.98;                       % CFL condition
dt_min=1e-5;                    % Minimal timestep
Nsteps_jumped= 5000;            % Maximal number of stored time steps 
delta=0.001;                    % Small parameter for the entropic remede in case of Gibbs oscillations in shocks 

f_ent=@(x) (x.*(x>delta)+(x.^2./(2*delta)+delta/2).*(x<delta)); % Function for the entropic Remede (See Allaire Cocquel)

fprintf(1,'Number of space points: N=%d\n',N);



k=6; %length of the cut in units of number of points
f_cut=ones(N,1); %cutting func
f_cut((N-1)/2-k:(N-1)/2+k,1)=0; %cutting func contd. 




%Usefull macros


%%%%%%%%%%%%%%%%%%%%%%%%
%%% ALLOCATION %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%

dy=1/(N-1);                            % Spacing of the grid
Y=linspace(dy/2,1-dy/2,N); Y=Y';            % defining the grid 
Yd=linspace(0,1,N+1);                  % defining the dual grid


Sig=zeros(N+2,1);                         %Stress
Sig_sav=zeros(N+2,Nsteps_jumped);         %saved stress

v=zeros(N+1,1);                           %Velocity
v_sav=zeros(N+1,Nsteps_jumped);           % saved velocity

caL=zeros(N,2);                         % Density of attached myosin mulptipilied by L 
ca_sav=zeros(N,Nsteps_jumped);           % saved attached myosin concentration

cdL=zeros(N,2);                         % Density of detached myosin mulptipilied by L 
cd_sav=zeros(N,Nsteps_jumped);           % saved detached myosin concentration

rhoL=zeros(N,2);                         % Density of actin mulptipilied by L 
rho_sav=zeros(N,Nsteps_jumped);          % saved actin density


TimeS=zeros(1,Nsteps_jumped);          % saved time steps
r=zeros(1,2);                          %rear
r_sav=zeros(1,Nsteps_jumped);
f=zeros(1,2);                          %front
f_sav=zeros(1,Nsteps_jumped);


Fa=zeros(N+1,1);                        % Flux of attached myosin 
Fd=zeros(N+1,1);                        % Flux of detached myosin 
Frho=zeros(N+1,1);                      % Flux of actin 
one=ones(N,1);                          % Macro



%f_cut((N-1)/2,1)=0;
%f_cut(51,1)=0;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% INITIALIZATION OF THE SOLUTION AT T=0 %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Time=0;                               %actual time
TimeS(1,1)=0;                         %Saved times

ii = 1;
j=1;

                 
ca_sav(:,1)=ca_handle(ytox(Y,0,L));
cd_sav(:,1)=cd_handle(ytox(Y,0,L));
rho_sav(:,1)=rho_handle(ytox(Y,0,L));

caL(:,1)=ca_sav(:,1).*L;        %initial concentration for detached myosin times L 
cdL(:,1)=cd_sav(:,1).*L;        %initial concentration for detached myosin times L 
rhoL(:,1)=rho_sav(:,1).*L;        %initial density for attached myosin times L 


Jac=L*dy;
Jacc=Jac^2;
R=spdiags([-one,(2+xi*Jacc/eta).*one,-one],-1:1,N,N); %Tridiagonal matrix
R(1,1)=1+xi*Jacc/eta;R(N,N)=1+xi*Jacc/eta;

% Computation of the stress inside the layer. Hard device boundary conditions 
Sig(2:N+1,1)=R\(xi*Jacc/eta.*(-E*f_e(rhoL(:,1)./L)+chi*f_c(rhoL(:,1)./L,caL(:,1)./L))+D_m./sqrt(Jac)*randn(N,1)); Sig(1,1)=Sig(2,1); Sig(N+2,1)=Sig(N+1,1); %Computation of stress in side the cell
Sig_sav(:,1)=Sig;



v(:,1)=diff(Sig)./(Jac*xi);              %Computation of velocity                     
v_sav(:,1)=v(:,1);

% computation of the drift to solve the transport equations 
drift_eff=v(:,1);

% Chosing the time step according to the CFL condition
dt=min([CFL.*Jac./max(abs(drift_eff)+2*max([Diffuca,Diffucd,Diffurho])./Jac),dt_min]);    
fprintf(1,'dt: t=%f \n',dt);
          

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% GENERAL TIME STEPPING %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic;

while(Time<T_final)
    Time = Time + dt;
    j=j+1;


%Computation of fluxes 
 drift_ent=f_ent(abs(drift_eff(2:N,1)));       %Remede entropique pour choc stationnaire (cours Allaire-Coquel)
 Fa(2:N,1)=0.5*(drift_eff(2:N,1).*(caL(2:N,1)+caL(1:N-1,1))-(drift_ent+2*Diffuca./Jac).*(caL(2:N,1)-caL(1:N-1,1)));
 Fd(2:N,1)=-Diffucd./Jac.*(cdL(2:N,1)-cdL(1:N-1,1));
 Frho(2:N,1)=0.5*(drift_eff(2:N,1).*(rhoL(2:N,1)+rhoL(1:N-1,1))-(drift_ent+2*Diffurho./Jac).*(rhoL(2:N,1)-rhoL(1:N-1,1)));
 
 
%We impose no flux BC on actin
Frho(1)=0.0;
Frho(N+1)=0.0; 

%We impose no flux BC on motors
Fa(1)=0;
Fa(N+1)=0;   

Fd(1)=0;
Fd(N+1)=0; 




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%CUT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(Time>=T_cut-2*dt && Time<T_cut+2*dt )
caL(:,1)= caL(:,1).*f_cut;
%plot(Y,caL(:,1),'b.-');xlim([-0.01 1.01 ]) 
end  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%Euler scheme for actin and myosin densities. The scheme is explicit. 

rhoL(:,2)=rhoL(:,1);  % actin - here the dynamics of actin have been removed. 

%rhoL(:,2)=rhoL(:,1)-dt*diff(Frho)./Jac+L*S_rho(rhoL(:,1)/L)*dt;  % actin - here we can turn on the dynamics of actin


caL(:,2)=caL(:,1)-dt*(diff(Fa)./Jac)+L*(k_on(Y).*cdL(:,1)/L-k_off(Y).*caL(:,1)/L)*dt; % attached motors
cdL(:,2)=cdL(:,1)-dt*(diff(Fd)./Jac)-L*(k_on(Y).*cdL(:,1)/L-k_off(Y).*caL(:,1)/L)*dt; % detached motors







% Computation of stress again.
Sig(2:N+1,1)=R\(xi*Jacc/eta.*(-E*f_e(rhoL(:,2)./L)+chi*f_c(rhoL(:,2)./L,caL(:,2)./L))+D_m./sqrt(Jac)*randn(N,1)); Sig(1,1)=Sig(2,1); Sig(N+2,1)=Sig(N+1,1); %Computation of stress in side the cell

v(:,1)=diff(Sig)./(Jac*xi);              % computation of velocity                     

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%Saving some time steps%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
 if ~(mod(j,dt_jump))
        ii = ii + 1;
        v_sav(:,ii)=v(:,1); % saved actin velocity
        Sig_sav(:,ii)=Sig(:,1); % saved actin meshwork stress
        rho_sav(:,ii)=rhoL(:,2)./L; % saved density of actin
        ca_sav(:,ii)=caL(:,2)./L; % saved concentration of attached motors
        cd_sav(:,ii)=cdL(:,2)./L; % saved concentration of detached motors
        TimeS(1,ii)=Time; % saved times
        fprintf(1,'Time: t=%f \n',Time);
 end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Preparing the next time step %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 

% effective flux
drift_eff=v(:,1); 

% densities updates
rhoL(:,1)=rhoL(:,2);
caL(:,1)=caL(:,2);
cdL(:,1)=cdL(:,2);


% adapting the time step with the CFL condition
dt=min([CFL.*Jac./max(abs(drift_eff)+2*max([Diffuca,Diffucd,Diffurho])./Jac),dt_min]); 

end
   
toc

% To vizualize the results of main

%{
for j=1:ii
close all
subplot(2,2,1); 
plot(Yd,v_sav(:,j),'b.-');xlim([-0.01 1.01]) % Velocity
ylabel('Velocity','Fontsize',15);
xlabel('y','Fontsize',15);
set(gca,'Fontsize',15);

subplot(2,2,2); 
plot([-dy;Y;1+dy],Sig_sav(:,j),'b.-');xlim([-0.01 1.01]) % Stress
ylabel('Stress','Fontsize',15);
xlabel('y','Fontsize',15);

subplot(2,2,3); 
plot(Y,cd_sav(:,j),'b.-');xlim([-0.01 1.01 ]) % myosine density
ylabel('Detached motors density','Fontsize',15);
xlabel('y','Fontsize',15);
set(gca,'Fontsize',15);

subplot(2,2,4); 
plot(Y,ca_sav(:,j),'b.-');xlim([-0.01 1.01 ]) % myosine density
ylabel('Attched motors density','Fontsize',15);
xlabel('y','Fontsize',15);
set(gca,'Fontsize',15);

% subplot(5,1,5);
% plot(Y,rho_sav(:,j),'b.-');%axis([-0.01 1.01 min(rho_sav(:,j)) max(rho_sav(:,j))]) % actin density
% ylabel('Actin density','Fontsize',15);
% xlabel('y','Fontsize',15);
% set(gca,'Fontsize',15);

fprintf(1,'Time : =%f \n',TimeS(1,j));
pause(0.2)
end
%}

space= linspace (0, N+1, 1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%        SAVE             %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
fid = fopen("myo_int.txt", 'w+');
fprintf(fid, '#time #space.pt #concentration \n');
for j=1:ii
for i=1:N
fprintf(fid, '%f %f %f \n', TimeS(1,j), i/N,  ca_sav(i,j));
end
fprintf(fid, '\n');
end
fclose(fid);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%        SAVE             %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
fid = fopen("myo_v.txt", 'w+');
fprintf(fid, '#time #space.pt #velocity \n');
for j=1:ii
for i=1:N
fprintf(fid, '%f %f %f \n', TimeS(1,j), i/N,  v_sav(i,j));
end
fprintf(fid, '\n');
end
fclose(fid);


fprintf(1,'Done!\n');

