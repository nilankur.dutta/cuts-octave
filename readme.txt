1. RUN main.m on octave / matlab
2. Two files are generated : 
	myo_int.txt with the kymograph data for concentration of detached motors. 
	myo_v.txt with the kymograph data for velocity 
3. k_on and k_off may be accessed through the changing the formulae on line 45 and 46 of main.m 
4. For more ways to modify the code refer comments in main.m



