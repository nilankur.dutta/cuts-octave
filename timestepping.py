# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
from scipy.sparse import spdiags


def timestepping(artvel=False):
    """Solves the system of PDEs

    TODO: add PDEs.

    with boundary conditions:

    TODO: add BCs

    as described in

    TODO: add paper or reference.

    Args:
        artvel (Boolean): Flat whether to use artificially set velocity.

    Returns:
        N (int): The number of spatial sampling points.
        ii (int): The number of dumps.
        TimeS (np.array): The times at which the dumps are taken.
        ca_sav (np.array): Array of concentrations of attached actin.
        cd_sav (np.array): Array of concentrations of detached actin.
        a_sav (np.array): Array of 'features'.
        v_sav (np.array): Array of velocities.
    """
    # Physical parameters.
    T_final = 0.1
    T_cut = 0.01

    # Inital conditions.
    L = 1.0  # Curvilinear length between the two poles.

    # Initial distribution of attached motors.
    def ca_handle(x):
        return stats.uniform.pdf(x, 0, L) * 20

    # Initial distribution of dettached motors.
    def cd_handle(x):
        return stats.uniform.pdf(x, 0, L) * 20

    # Initial distribution of actin
    def rho_handle(x):
        return stats.uniform.pdf(x, 0, L) * 20

    # Initial distribution of features.
    def a_handle(x):
        return stats.uniform.pdf(x, 0, L) * 20 \
            + 1 * np.sin(50 * L * x) / (0.5 * L)
            # + 10 * np.sin(100 * L * x + np.cos(100 * L * x)) / (0.5 * L)

    # Artificial velocity paramters.
    # tau_t = 1  # Coefficient of exponent for cut-
    # v_0 = 5.1  # Velocity at cut 'lips'.
    # a_0 = 2.1  # Initial cut width.

    # Define parameters for artificial velocity.
    c0 = 0.05
    v0 = 5
    tau0 = 0.05
    ell1 = 0.1

    # Mechanical paramters.
    # Viscosity of the cortex.
    eta = 0.5
    # Friction with the (sort of static ?) membrane.
    xi = 0.15
    # Elastic modulus of the actine meshwork (or compressibility).
    E = 1.0
    # Contractility modulus.
    chi = 0.5
    # Diffusion coefficient of attached mysoin.
    Diffuca = 0.0
    # Diffusion coefficient of the detached myosin.
    Diffucd = 0.0
    # Diffusion coefficient of actin
    # (interpretation as a non local source term).
    Diffurho = 0.0000
    # 'reference' actin density of elasticity.
    rho_e = 1.0
    # 'reference' myosin concentration of contraction.
    c_c = 1.0

    # Chemistry.
    rho_0 = 1.0  # Target density of actin.
    tau = 1.0  # Tunover time of actin.

    # Rate of myosin attachement (here centered at the equator).
    def k_on(y):
        return 1.000  # +1.0*exp(-(y-0.5).^2/0.05)

    # Rate of myosin detachement (here high at the poles).
    def k_off(y):
        return 3.0  # *(exp(-y.^2/0.05)+exp(-(y-1.0).^2/0.05))

    # Mechanical noise (small biological gaussian noise in the force equation).
    D_m = 0.0 #1.0e-8  # Small Gaussian noise on c.

    # Rheology functions.
    # Elastic  compressibility, here infinite compressibility (f_e=0).
    def f_e(rho):
        return 0  # rho/rho_e

    # Contraction (here only depending on attached motors).
    def f_c(rho, c):
        return c / c_c

    # Source term of actin.
    def S_rho(rho):
        return (rho_0 - rho) / tau

    def ytox(y, r, f):
        return y * (f - r) + r

    # Numerical parameters.
    # Number of jumped snapshots of the solution before one is save.
    dt_jump = 100
    N = 101  # Number of space points.
    N_h = int(N / 2.0)
    CFL = 0.98  # CFL condition.
    dt_min = 1.0e-5  # Minimal timestep.
    # Maximal number of stored time steps.
    Nsteps_jumped = int(10 * T_final * 1.0 / (dt_min * dt_jump))
    # Small parameter for the entropic remede in case of
    # Gibbs oscillations in shocks.
    delta = 0.001

    # Function for the entropic Remede (See Allaire Cocquel).
    def f_ent(x):
        return (x * (x > delta)
                + (x**2 / (2 * delta) + delta / 2) * (x < delta))

    print('Number of space points: N=', N)
    print('NSteps_jumped= ', Nsteps_jumped)

    # Length of the cut in units of number of points.
    k = 2
    # Cutting func.
    f_cut = np.ones(N)
    # Cutting func contd.
    f_cut[(N - 1) // 2 - k:(N - 1) // 2 + k] = 0

    # Allocation.
    # Spacing of the grid.
    dy = 1.0 / (N - 1)

    Y = np.linspace(dy / 2, 1 - dy / 2, num=N)
    # Defining the grid.
    Y = Y.transpose()
    # Defining the dual grid.
    Yd = np.linspace(0, 1, num=N + 1)

    # Stress.
    Sig = np.zeros(N + 2)
    # Saved stress.
    Sig_sav = np.zeros((N + 2, Nsteps_jumped))

    # Velocity.
    v = np.zeros(N + 1)
    # Saved velocity.
    v_sav = np.zeros((N + 1, Nsteps_jumped))

    # Density of attached myosin mulptipilied by L.
    caL = np.zeros((N, 2))
    # Saved attached myosin concentration.
    ca_sav = np.zeros((N, Nsteps_jumped))

    # Density of detached myosin mulptipilied by L.
    cdL = np.zeros((N, 2))
    # Saved detached myosin concentration.
    cd_sav = np.zeros((N, Nsteps_jumped))

    # Density of actin mulptipilied by L.
    rhoL = np.zeros((N, 2))
    # Saved actin density.
    rho_sav = np.zeros((N, Nsteps_jumped))

    # Features, mulptipilied by L.
    aL = np.zeros((N, 2))
    # Saved features.
    a_sav = np.zeros((N, Nsteps_jumped))

    # Saved time steps.
    TimeS = np.zeros((1, Nsteps_jumped))
    # Rear.
    r = np.zeros((1, 2))
    r_sav = np.zeros((1, Nsteps_jumped))
    # Front.
    f = np.zeros((1, 2))
    f_sav = np.zeros((1, Nsteps_jumped))

    # Flux of attached myosin.
    Fa = np.zeros(N + 1)
    # Flux of detached myosin.
    Fd = np.zeros(N + 1)
    # Flux of actin.
    Frho = np.zeros(N + 1)
    Faf = np.zeros(N + 1)
    Fav = np.zeros(N + 1)
    # Macro.
    one = np.ones(N)

    # Initrialization of the soljution at T=0.
    # Actual time.
    Time = 0.0
    # Saved times.
    TimeS[0, 0] = 0.0

    ii = 0
    j = 1

    ca_sav[:, 0] = ca_handle(ytox(Y, 0, L))
    cd_sav[:, 0] = cd_handle(ytox(Y, 0, L))
    rho_sav[:, 0] = rho_handle(ytox(Y, 0, L))
    a_sav[:, 0] = a_handle(ytox(Y, 0, L))

    # Initial concentration for detached myosin times L.
    caL[:, 0] = ca_sav[:, 0] * L
    # Initial concentration for detached myosin times L.
    cdL[:, 0] = cd_sav[:, 0] * L
    # Initial density for attached myosin times L.
    rhoL[:, 0] = rho_sav[:, 0] * L
    # Initial concentration for features times L.
    aL[:, 0] = a_sav[:, 0] * L

    Jac = L * dy
    Jacc = Jac**2

    data = np.array([-one, (2 + xi * Jacc / eta) * one, -one])
    diags = np.array([-1, 0, 1])
    # Tridiagonal matrix.
    R = spdiags(data, diags, N, N).toarray()
    R[0, 0] = 1 + xi * Jacc / eta
    R[N - 1, N - 1] = 1 + xi * Jacc / eta

    stres = np.zeros(N)
    stres = ((xi * Jacc / eta) * chi * f_c(rhoL[:, 0] / L, caL[:, 0] / L)) \
        + np.divide(D_m / np.sqrt(Jac), np.random.randn(N))

    # Computation of the stress inside the layer.
    temp = np.linalg.solve(R, stres)

    Sig[1:N + 1] = temp[0:N]
    Sig[0] = Sig[1]

    # Computation of stress in side the cell.
    Sig[N + 1] = Sig[N]

    Sig_sav[:, 0] = Sig

    # Computation of velocity.
    v = np.divide(np.diff(Sig, axis=0), (Jac * xi))
    v_sav[:, 0] = v[:]

    # Computation of the drift to solve the transport equations.
    drift_eff = v

    # Chosing the time step according to the CFL condition

    dt = np.min([CFL * Jac / np.max(np.abs(drift_eff)
                + 2 * np.max([Diffuca, Diffucd, Diffurho]) / Jac), dt_min])
    print('dt: t=', dt)

    # General time stepping.
    plt.ion()
    while Time <= T_final:
        Time = Time + dt
        j = j + 1

        # Artificial velocity field.
        if(artvel):
            def gammavel(t):
                return v0 * np.exp(-t / tau0)

            def gamma(t):
                return c0 + tau0 * v0 * (1 - np.exp(-t / tau0))

            def vel(t, x):
                gvel = gammavel(t)
                g = gamma(t)

                if abs(x) <= gamma(t):
                    v = gvel * abs(x) / g
                else:
                    v = gvel * np.exp(-abs(abs(x) - g) / ell1)

                return -v if x < 0 else v

            v = [vel(Time, x * dy - 0.5) for x in range(N + 1)]
            drift_eff = v

#            ex = np.exp(-Time * 1.0 / tau_t)
#
#            v_a = (v_0 * ex)
#            a_a = (a_0 + tau_t * v_0 * (1 - ex)) / (a_0 + tau_t * v_0)
#            n_a_a = int(a_a * (N + 1))
#
#            if(n_a_a >= N_h):
#                print('cut too big for simulation domain')
#                break
#
#            if(n_a_a < 2):
#                print('cut too small for simulation domain')
#                break
#
#            grid1 = N_h - n_a_a
#            grid2 = N_h + n_a_a + 1
#
#            point1 = grid1 * 1.0 / (N + 1)
#            point2 = grid2 * 1.0 / (N + 1)
#
#            ex1 = (np.log(v_a) + 10.0) / point1
#            ex2 = (10.0 + np.log(v_a)) / (1 - point2)
#
#            v[0:N_h] = -np.exp(ex1 * Yd[0:N_h] - 10.0)
#            v[N_h + 1:N + 1] = np.exp(-(ex2 * Yd[N_h + 1:N + 1]
#                                      - (ex2 - 10.0)))
#            v[grid1:grid2 + 1] = np.linspace(-v_a, v_a, 2 * n_a_a + 2)
#            drift_eff = v
#
#            caL[grid1:grid2+1, 0] = 0.0
#            cdL[grid1:grid2+1, 0] = 0.0
            # aL[grid1:grid2+1, 0] = 0.0

        # Remede entropique pour choc stationnaire (cours Allaire-Coquel).
        drift_ent = f_ent(np.abs(drift_eff[1:N]))
        Fa[1:N] = 0.5 * (np.multiply(drift_eff[1:N], (caL[1:N, 0]+caL[0:N - 1, 0])) - np.multiply((drift_ent + 2 * Diffuca / Jac), (caL[1:N, 0]-caL[0:N - 1, 0])))
        Fd[1:N] = np.multiply((-Diffucd / Jac), (cdL[1:N, 0]-cdL[0:N - 1, 0]))

        Frho[1:N] = 0.5 * (np.multiply(drift_eff[1:N], (rhoL[1:N, 0] + rhoL[0:N - 1, 0])) - np.multiply((drift_ent + 2 * Diffurho / Jac), (rhoL[1:N, 0]-rhoL[0:N - 1, 0])))

        Faf[1:N] = 0.5 * (np.multiply(drift_eff[1:N], (aL[1:N, 0] + aL[0:N - 1, 0])))
        Fav[0:N + 1] = (drift_eff[0:N + 1])

        # We impose no flux BC on actin.
        Frho[0] = 0.0
        Frho[N] = 0.0

        # We impose no flux BC on motors.
        Fa[0] = 0.0
        Fa[N] = 0.0

        Fd[0] = 0.0
        Fd[N] = 0.0

        # We impose no flux BC on features.
        Faf[0] = 0.0
        Faf[N] = 0.0

        # Cut.
        if(not artvel):
            if(Time >= T_cut - 1.0 * dt and Time < T_cut + 1.0 * dt):
                caL[:, 0] = np.multiply(caL[:, 0], f_cut)
                # aL[:, 0] = np.multiply(aL[:, 0], f_cut)

        # Actin - here the dynamics of actin have been removed.
        rhoL[:, 1] = rhoL[:, 0]

        # Detached motors.
        cdL[:, 1] = cdL[:, 0] - dt * (np.diff(Fd, axis=0) / Jac) - L * (np.multiply(k_on(Y), cdL[:, 0] / L) - np.multiply(k_off(Y), caL[:, 0] / L)) * dt
        # Attached motors
        caL[:, 1] = caL[:, 0] - dt * (np.diff(Fa, axis=0) / Jac) + L * (aL[:, 0] * 1.0 / (L * 1.0) - np.multiply(k_off(Y), caL[:, 0] / L)) * dt

        aL[:, 1] = aL[:, 0] - dt * (np.diff(Faf, axis=0) / Jac) + (np.multiply(aL[:, 0], np.diff(Fav, axis=0) / Jac) ) * dt

        # Mechanics.
        if(not artvel):
            stres = np.zeros(N)
            stres = ((xi * Jacc / eta) * chi * f_c(rhoL[:, 0] / L, caL[:, 0] / L)) + np.divide(D_m / np.sqrt(Jac), np.random.randn(N))
            # Computation of the stress inside the layer.
            temp = np.linalg.solve(R, stres)

            Sig[1:N + 1] = temp[0:N]
            Sig[0] = Sig[1]
            # Computation of stress in side the cell.
            Sig[N+1] = Sig[N]

            v = np.divide(np.diff(Sig, axis=0), (Jac * xi))

        # Saving some time steps.
        if (j % dt_jump == 0):
            ii = ii + 1
            # Saved actin velocity.
            v_sav[:, ii] = v[:]
            # Saved actin meshwork stress.
            Sig_sav[:, ii] = Sig[:]
            # Saved density of actin.
            rho_sav[:, ii] = rhoL[:, 1] / L
            # Saved concentration of attached motors.
            ca_sav[:, ii] = caL[:, 1] / L
            # Saved concentration of detached motors.
            cd_sav[:, ii] = cdL[:, 1] / L
            # Saved features.
            a_sav[:, ii] = aL[:, 1] / L
            # Saved times.
            TimeS[0, ii] = Time
            print('Time=', Time)

        # Preparing the next time step.

        # Effective flux.
        drift_eff = v

        # Densities updates.
        rhoL[:, 0] = rhoL[:, 1]
        caL[:, 0] = caL[:, 1]
        cdL[:, 0] = cdL[:, 1]
        aL[:, 0] = aL[:, 1]

        # Adapting the time step with the CFL condition.
        dt = np.min([CFL * Jac / np.max(np.abs(drift_eff) + 2 * np.max([Diffuca, Diffucd, Diffurho]) / Jac), dt_min])

        # Finish if array bounds exceeded.
        if(ii >= Nsteps_jumped - 2):
            print("array bounds exceeded at time t=", T_final)
            break

    return N, ii, TimeS, ca_sav, cd_sav, a_sav, v_sav
