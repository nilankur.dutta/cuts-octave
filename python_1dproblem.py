#!/usr/bin/python
import matplotlib.pyplot as plt
import math
import numpy as np
from timestepping import timestepping

# Settings.
artvel = False

# Run time stepping algorithm.
N, ii, TimeS, ca_sav, cd_sav, a_sav, v_sav = timestepping(artvel)

# Save concentration.
f1 = open('myo_int_py.txt', 'w')
f1.write('#time #space.pt #concentration\n')
for j in range(0, ii):
    for i in range(0, N):
        f1.write('{0} {1} {2} \n'.format(TimeS[0, j],
                                         1.0 * i / N,
                                         ca_sav[i, j] + a_sav[i, j]))
    f1.write('\n')
f1.close()


# Save velocity.
f2 = open('myo_vel_py.txt', 'w')
f2.write('#time #space.pt #velocity\n')
for j in range(0, ii):
    for i in range(0, N):
        f2.write('{0} {1} {2} \n'.format(TimeS[0, j],
                                         1.0 * i / N,  v_sav[i, j]))
    f2.write('\n')
f2.close()

print('Done!\n')

# Plot results.
rng = range(ii)

fig, ax = plt.subplots(figsize=(10, 10))
cax = ax.imshow(a_sav[:, rng])
ax.set_title('a_sav')
fig.colorbar(cax, orientation='horizontal')
plt.show()
fig.savefig('a_sav.png')
plt.close(fig)

fig, ax = plt.subplots(figsize=(10, 10))
cax = ax.imshow(ca_sav[:, rng])
ax.set_title('ca_sav')
fig.colorbar(cax, orientation='horizontal')
plt.show()
fig.savefig('ca_sav.png')
plt.close(fig)

fig, ax = plt.subplots(figsize=(10, 10))
cax = ax.imshow(cd_sav[:, rng])
ax.set_title('cd_sav')
fig.colorbar(cax, orientation='horizontal')
plt.show()
fig.savefig('cd_sav.png')
plt.close(fig)

fig, ax = plt.subplots(figsize=(10, 10))
cax = ax.imshow(ca_sav[:, rng] + a_sav[:, rng])
ax.set_title('ca_sav + a_sav')
fig.colorbar(cax, orientation='horizontal')
plt.show()
fig.savefig('ca_a_sav.png')
plt.close(fig)

fig, ax = plt.subplots(figsize=(10, 10))
cax = ax.imshow(v_sav[:, rng])
ax.set_title('v_sav')
fig.colorbar(cax, orientation='horizontal')
plt.show()
fig.savefig('v_sav.png')
plt.close(fig)

# Plot velocity profile.
fig = plt.figure()
t = dict()
samples = [math.floor(x) for x in np.linspace(0, ii, num=5)]
for k in samples:
    t[k], = plt.plot(v_sav[:, k], label='t={0}'.format(k))
plt.legend(handles=list(t.values()))
plt.show()
fig.savefig('v_sav_profile.png')
plt.close(fig)
